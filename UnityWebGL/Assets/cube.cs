﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

[System.Serializable]
public class Trans
{
    public float x;
    public float y;
    public float z;
}
public class cube : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void CubeClick();
    // Start is called before the first frame update
    void Start()
    {
#if !UNITY_EDITOR && UNITY_WEBGL
        UnityEngine.WebGLInput.captureAllKeyboardInput = false; // or true
#endif
    }

    // Update is called once per frame
    void Update()
    {
        GameObject cube = GameObject.Find("Cube1");
    }

    public void Triger()
    {
        Debug.Log("Trigger");
        CubeClick();
    }

    //位置
    void changePosition(string json)
    {
        var trans = JsonUtility.FromJson<Trans>(json);
        GameObject cube = GameObject.Find("Cube1");
        cube.transform.position = new Vector3(trans.x, trans.y, trans.z);
    }

    //旋转
    void changeRotation(string json)
    {
        var trans = JsonUtility.FromJson<Trans>(json);
        GameObject cube = GameObject.Find("Cube1");
        cube.transform.eulerAngles = new Vector3(trans.x, trans.y, trans.z);
    }

    //拉伸(改变大小)
    void changeScale(string json)
    {
        var trans = JsonUtility.FromJson<Trans>(json);
        GameObject cube = GameObject.Find("Cube1");
        cube.transform.localScale = new Vector3(trans.x, trans.y, trans.z);
    }
}